package com.eaio.stringsearch;

import static org.junit.Assert.*;

import org.junit.Test;

public class BDNMTest {
    
    @Test
    public void testBNDMPatternNotFound() {
	// Technique used: input space partitioning
	//
	// This test case represents the block of inputs to the
	// BNDM implementation of StringSearch.searchString
	// that contain:
	//
	// - a non-null pattern
	// - a non-null string
	// - the pattern is NOT found in the string.
	
	StringSearch ss = new BNDM();
	String str = "helloworld";
	String pattern = "se6367";
	
	int location = ss.searchString(str, pattern);
	
	assertEquals(-1, location);
    }
    
    @Test
    public void testBNDMPatternLongerThanString() {
	// Technique used: input space partitioning
	//
	// This test case represents the block of inputs to the
	// BNDM implementation of StringSearch.searchString
	// that contain:
	//
	// - a non-null pattern
	// - a non-null string
	// - the pattern is NOT found in the string.
	
	MismatchSearch ss = new ShiftOrMismatches();
	String str = "helloworld";
	String pattern = "hel1ohel1o";
	
	int[] locations = ss.searchString(str, pattern, 1);
	
	for (int i = 0; i < locations.length; i++)
	    System.out.println(locations[i]);
	
	assertArrayEquals(new int[] { -1, 0 }, locations);
    }
}
